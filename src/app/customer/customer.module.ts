import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerRoutingModule } from './customer-routing.module';
import { CustomerComponent } from './customer.component';

import { RegistrationComponent } from './registration/registration.component';
import { DashboardComponent } from './dashboard/dashboard.component';

 

@NgModule({
  declarations: [
    CustomerComponent,
  
    RegistrationComponent,
    DashboardComponent
  ],
  imports: [
    CommonModule,
    CustomerRoutingModule
  ],
  providers:[
    
  ]
})
export class CustomerModule { }
